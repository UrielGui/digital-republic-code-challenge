<h1 align="center">Digital Republic - Code Challenge</h1>

<p align="center">
  <a href="#rocket-technologies">Technologies</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#information_source-how-to-use">How To Use</a>&nbsp;&nbsp;
</p>

![Digital Republic - Code Challenge](https://i.imgur.com/jjIEU9F.jpg)

### :rocket: Technologies

- [React JS](https://reactjs.org/)
- [React Hooks](https://reactjs.org/docs/hooks-intro.html)
- [Styled-Components](https://styled-components.com/)
- [React-Icons](https://react-icons.netlify.com/)

## :information_source: How To Use

1. Clone repo

```bash
git clone https://gitlab.com/UrielGui/digital-republic-code-challenge.git
```

2. Install Dependencies

```bash
npm install
```

3. Run the app

```bash
npm start
Open http://localhost:3000 to view it in the browser.
```
