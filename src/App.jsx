import React, { useContext } from "react";
import * as Styled from "Styled";
import { StageContext } from "contexts/appStage";
import Welcome from "screens/Welcome";
import Wall from "screens/Wall";
import Door from "screens/Door";
import Window from "screens/Window";
import End from "screens/End";

export default function App() {
  const [stage] = useContext(StageContext);

  return (
    <Styled.Section>
      <Styled.Content>
        {stage.appStage === "Start" && <Welcome />}
        {stage.appStage === "Wall" && <Wall />}
        {stage.appStage === "Door" && <Door />}
        {stage.appStage === "Window" && <Window />}
        {stage.appStage === "End" && <End />}
      </Styled.Content>
    </Styled.Section>
  );
}
