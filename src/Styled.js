import styled from "styled-components";
import { theme } from "assets/styles/Theme";

export const Section = styled.section`
  width: 100%;
  height: 100%;
  background: ${theme.colors.primary};
  min-height: 38rem;
  border-radius: 0rem 0rem 5rem 5rem;
`;

export const Content = styled.div`
  padding: 5rem 0 5rem 0;
  display: flex;
  flex-direction: column;
  place-items: center;
`;
