import React from "react";
import styled from "styled-components";
import { theme } from "assets/styles/Theme";
import PropTypes from "prop-types";

export const InputStyle = styled.input`
  border-radius: 5px;
  margin: 5px 0;
  height: 2rem;
  background: ${theme.colors.inputPlaceholder};
  padding: 20px;
  color: #fff;

  ::placeholder {
    color: #fff;
  }
}
`;

export const InputWarningStyle = styled.small`
  color: ${theme.colors.warningLight};
`;

export function Input(props) {
  return (
    <InputStyle
      name={props.name}
      disabled={props.disabled}
      type={props.type}
      value={props.value}
      min={props.min}
      max={props.max}
      placeholder={props.placeholder}
      onChange={props.onChange}
    />
  );
}

export function InputWarning(props) {
  return <InputWarningStyle>{props.text}</InputWarningStyle>;
}
Input.propTypes = {
  name: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.node,
  min: PropTypes.number,
  max: PropTypes.number,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};

InputWarning.propTypes = {
  text: PropTypes.string,
};
