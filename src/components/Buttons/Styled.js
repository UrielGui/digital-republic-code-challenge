import styled from "styled-components";
import { theme } from "assets/styles/Theme";

export const DefaultButton = styled.button`
  padding: 20px 80px;
  border-radius: 7px;
  background: ${(props) => (props.background ? props.background : theme.colors.success)};
  color: #fff;
  font-size: 2rem;
  font-weight: bold;

  :hover {
    opacity: 80%;
  }
`;
