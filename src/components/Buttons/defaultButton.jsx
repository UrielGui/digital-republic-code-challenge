import React from "react";
import PropTypes from "prop-types";
import * as Styled from "./Styled";

export default function DefaultButton(props) {
  return (
    <>
      <Styled.DefaultButton background={props.background} type={props.type} onClick={props.onClick}>
        {props.children}
      </Styled.DefaultButton>
    </>
  );
}

DefaultButton.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
  onClick: PropTypes.func,
  background: PropTypes.string,
};
