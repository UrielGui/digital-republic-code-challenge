import React, { useContext, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import * as Styled from "./Styled";
import { MeasurementsContext } from "contexts/measurements";
import { Input } from "components/input";
import useMinMaxNumber from "hooks/useMinMaxNumber";
import SquareMeter from "./squareMeter";
export default function Measurements(props) {
  const [state, dispatch] = useContext(MeasurementsContext);
  const formRef = useRef();
  const min = 1;
  const max = 50;

  function updateState(e) {
    if (props.checked) {
      for (let i = 1; i < 5; i++) {
        dispatch({
          field: e.target.name.includes("wallWidth") ? `wallWidth${i}` : `wallHeight${i}`,
          payload: useMinMaxNumber(min, max, e.target.value),
        });
      }
    }
    dispatch({
      field: e.target.name,
      payload: useMinMaxNumber(min, max, e.target.value),
    });
  }

  function resetState() {
    for (let i = 1; i < 5; i++) {
      dispatch({
        field: `wallWidth${i}`,
        payload: 0,
      });
      dispatch({
        field: `wallHeight${i}`,
        payload: 0,
      });
      return formRef.current.reset();
    }
  }

  function squareMeterWallValue() {
    dispatch({
      field: "squareMeterWall1",
      payload: state.wallHeight1 * state.wallWidth1,
    });
    dispatch({
      field: "squareMeterWall2",
      payload: state.wallHeight2 * state.wallWidth2,
    });
    dispatch({
      field: "squareMeterWall3",
      payload: state.wallHeight3 * state.wallWidth4,
    });
    dispatch({
      field: "squareMeterWall4",
      payload: state.wallHeight4 * state.wallWidth4,
    });
  }

  useEffect(() => {
    if (props.checked) {
      resetState();
    }
  }, [props.checked, props.setChecked]);

  return (
    <>
      <Styled.Form ref={formRef}>
        <Styled.Content>
          <Styled.InputContent>
            <h2>{props.checked ? "Medidas das paredes" : "Medidas da 1º parede"}:</h2>
            <Input
              name="wallWidth1"
              type={"number"}
              min={1}
              max={50}
              value={state.wallWidth1 ? state.wallWidth1 : ""}
              placeholder="Digite aqui a largura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <Input
              name="wallHeight1"
              type={"number"}
              min={1}
              max={50}
              value={state.wallHeight1 ? state.wallHeight1 : ""}
              placeholder="Digite aqui a altura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <SquareMeter
              wallHeight={state.wallHeight1}
              wallWidth={state.wallWidth1}
              squareMeterWallValue={squareMeterWallValue}
            />
          </Styled.InputContent>
          <Styled.InputContent checked={props.checked}>
            <h2>{props.checked ? "Medidas das paredes" : "Medidas da 2º parede"}:</h2>
            <Input
              name="wallWidth2"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallWidth2 ? state.wallWidth2 : ""}
              placeholder="Digite aqui a largura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <Input
              name="wallHeight2"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallHeight2 ? state.wallHeight2 : ""}
              placeholder="Digite aqui a altura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <SquareMeter
              wallHeight={state.wallHeight2}
              wallWidth={state.wallWidth2}
              squareMeterWallValue={squareMeterWallValue}
            />
          </Styled.InputContent>
        </Styled.Content>
        <Styled.Content>
          <Styled.InputContent checked={props.checked}>
            <h2>{props.checked ? "Medidas das paredes" : "Medidas da 3º parede"}:</h2>
            <Input
              name="wallWidth3"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallWidth3 ? state.wallWidth3 : ""}
              placeholder="Digite aqui a largura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <Input
              name="wallHeight3"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallHeight3 ? state.wallHeight3 : ""}
              placeholder="Digite aqui a altura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <SquareMeter
              wallHeight={state.wallHeight3}
              wallWidth={state.wallWidth3}
              squareMeterWallValue={squareMeterWallValue}
            />
          </Styled.InputContent>
          <Styled.InputContent checked={props.checked}>
            <h2>{props.checked ? "Medidas das paredes" : "Medidas da 4º parede"}:</h2>
            <Input
              name="wallWidth4"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallWidth4 ? state.wallWidth4 : ""}
              placeholder="Digite aqui a largura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <Input
              name="wallHeight4"
              disabled={props.checked}
              type={"number"}
              min={1}
              max={50}
              value={state.wallHeight4 ? state.wallHeight4 : ""}
              placeholder="Digite aqui a altura da parede em metros"
              onChange={(e) => updateState(e)}
            />
            <SquareMeter
              wallHeight={state.wallHeight4}
              wallWidth={state.wallWidth4}
              squareMeterWallValue={squareMeterWallValue}
            />
          </Styled.InputContent>
        </Styled.Content>
      </Styled.Form>
    </>
  );
}

Measurements.propTypes = {
  checked: PropTypes.bool,
  setChecked: PropTypes.func,
};
