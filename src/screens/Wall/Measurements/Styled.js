import styled from "styled-components";
import { theme } from "assets/styles/Theme";
import { device } from "assets/styles/Breakpoints";

export const Content = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
  margin-bottom: 4rem;
  @media ${device.lg} {
    flex-direction: column;
    align-items: center;
  }
`;

export const InputContent = styled.div`
  display: flex;
  flex-direction: column;
  background: #733fc5;
  padding: 30px 60px;
  line-height: 3;
  width: 30%;
  border-radius: 10px;
  opacity: ${(props) => (props.checked ? "50%" : "100%")};
  @media ${device.lg} {
    margin: 1rem;
    width: 80%;
  }
  @media ${device.sm} {
    margin: 1rem;
    width: 90%;
    padding: 10px 20x;
  }
`;

export const Form = styled.form`
  width: 100%;
`;

export const SquareMeter = styled.p`
  color: ${(props) => (props.validation ? theme.colors.danger : "")};
`;
