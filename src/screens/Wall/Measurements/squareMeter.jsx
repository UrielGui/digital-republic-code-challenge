import React, { useEffect } from "react";
import PropTypes from "prop-types";
import * as Styled from "./Styled";
import { InputWarning } from "components/input";
export default function SquareMeter(props) {
  function measurementsValidation() {
    return props.wallWidth * props.wallHeight;
  }

  useEffect(() => {
    props.squareMeterWallValue();
  }, [measurementsValidation()]);

  return (
    <>
      <Styled.SquareMeter validation={measurementsValidation(props.wallWidth, props.wallHeight) > 50}>
        Medida: {measurementsValidation()}m²
      </Styled.SquareMeter>
      <InputWarning text={"*Medida máxima permitida: 50m²"} />
    </>
  );
}

SquareMeter.propTypes = {
  wallWidth: PropTypes.node,
  wallHeight: PropTypes.node,
  squareMeterWallValue: PropTypes.func,
};
