import React, { useContext, useState } from "react";
import * as Styled from "./Styled";
import { StageContext } from "contexts/appStage";
import DefaultButton from "components/Buttons/defaultButton";
import { theme } from "assets/styles/Theme";
import Measurements from "./Measurements";
import { MeasurementsContext } from "contexts/measurements";
import useNextScreen from "hooks/useNextScreen";

export default function Wall() {
  const [, dispatch] = useContext(StageContext);
  const [measurements] = useContext(MeasurementsContext);
  const [checked, setChecked] = useState(false);

  function nextScreen() {
    if (Object.values(measurements).find((val) => val > 50) && true) {
      return alert("O metro quadrado não pode ultrapassar 50m²");
    }
    return useNextScreen(measurements, dispatch, "door");
  }

  return (
    <>
      <Styled.Content>
        <h1>Vamos lá...</h1>
        <h2>Primeiramente eu preciso que você especifique a medida de cada parede da sua sala:</h2>
        <label htmlFor="checkbox">
          <h3>
            <Styled.CheckBox
              id="checkbox"
              type="checkbox"
              defaultChecked={checked}
              onClick={(e) => setChecked(e.currentTarget.checked)}
            />
            Marque essa opção se a largura e a altura de todas as paredes forem iguais
          </h3>
        </label>
      </Styled.Content>
      <Measurements checked={checked} setChecked={setChecked} />
      <Styled.ButtonDiv>
        <DefaultButton type={"button"} onClick={() => dispatch({ type: "start" })} background={theme.colors.warning}>
          Voltar
        </DefaultButton>
        <DefaultButton type={"button"} onClick={() => nextScreen()}>
          Avançar
        </DefaultButton>
      </Styled.ButtonDiv>
    </>
  );
}
