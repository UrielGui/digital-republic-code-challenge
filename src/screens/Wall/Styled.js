import styled from "styled-components";
import { theme } from "assets/styles/Theme";

export const Content = styled.div`
  padding-bottom: 4rem;
  text-align: center;
  margin-left: 1rem;
  margin-right: 1rem;

  h2,
  h3 {
    padding-top: 2rem;
  }

  h3 {
    color: ${theme.colors.success};
  }
`;

export const ButtonDiv = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
`;

export const CheckBox = styled.input`
  width: 30px;
  height: 30px;
  position: relative;
  top: 7px;
  right: 7px;
`;
