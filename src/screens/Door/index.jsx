import React, { useContext, useRef } from "react";
import * as Styled from "./Styled";
import { StageContext } from "contexts/appStage";
import { useStatesContext } from "contexts/states";
import { Input } from "components/input";
import useMinMaxNumber from "hooks/useMinMaxNumber";
import { MeasurementsContext } from "contexts/measurements";
import DefaultButton from "components/Buttons/defaultButton";
import { theme } from "assets/styles/Theme";
import useNextScreen from "hooks/useNextScreen";
import maxDoorsWindows from "utils/maxDoorsWindows";

export default function Door() {
  const [, dispatch] = useContext(StageContext);
  const { door1, door2, door3, door4, setDoor1, setDoor2, setDoor3, setDoor4, setDoorTotal } = useStatesContext();
  const formRef = useRef();
  const [measurements] = useContext(MeasurementsContext);

  const wallHeight = {
    door1: measurements.wallHeight1,
    door2: measurements.wallHeight2,
    door3: measurements.wallHeight3,
    door4: measurements.wallHeight4,
  };

  function updateState(e, door, setDoor) {
    setDoor(Number(useMinMaxNumber(0, 100, e.target.value, setDoor)));
    if (wallHeight[door] * 100 - 190 < 30) {
      setDoor(Number(useMinMaxNumber(0, 100, 0, setDoor)));
      alert("A altura da parede com a porta deve ser, no mínimo, 30 centímetros maior que a altura da porta");
    }
  }

  const total = door1 + door2 + door3 + door4;

  function nextScreen() {
    if (total * 2.7 * 100 > maxDoorsWindows(measurements)) {
      alert("O total de área das portas deve ser no máximo 50% da área de parede");
    } else {
      setDoorTotal(total * 2.7 * 100);
      useNextScreen("", dispatch, "window");
    }
  }

  return (
    <>
      <Styled.Title>
        <h1>Entendi...</h1>
        <h2>Agora eu preciso que você me informe quantas portas existem na sua sala, ok?</h2>
      </Styled.Title>
      <Styled.Form ref={formRef}>
        <Styled.Content>
          <Styled.InputContent>
            <h2>Portas na 1º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={door1}
              placeholder="Digite aqui o número de portas que existem na sua sala"
              onChange={(e) => updateState(e, "door1", setDoor1)}
            />
          </Styled.InputContent>
          <Styled.InputContent>
            <h2>Portas na 2º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={door2}
              placeholder="Digite aqui o número de portas que existem na sua sala"
              onChange={(e) => updateState(e, "door2", setDoor2)}
            />
          </Styled.InputContent>
        </Styled.Content>
        <Styled.Content>
          <Styled.InputContent>
            <h2>Portas na 3º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={door3}
              placeholder="Digite aqui o número de portas que existem na sua sala"
              onChange={(e) => updateState(e, "door3", setDoor3)}
            />
          </Styled.InputContent>
          <Styled.InputContent>
            <h2>Portas na 4º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={door4}
              placeholder="Digite aqui o número de portas que existem na sua sala"
              onChange={(e) => updateState(e, "door4", setDoor4)}
            />
          </Styled.InputContent>
        </Styled.Content>
      </Styled.Form>
      <Styled.ButtonDiv>
        <DefaultButton type={"button"} onClick={() => dispatch({ type: "wall" })} background={theme.colors.warning}>
          Voltar
        </DefaultButton>
        <DefaultButton type={"button"} onClick={() => nextScreen()}>
          Avançar
        </DefaultButton>
      </Styled.ButtonDiv>
    </>
  );
}
