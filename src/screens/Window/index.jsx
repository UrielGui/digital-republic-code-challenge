import React, { useContext, useRef } from "react";
import * as Styled from "./Styled";
import { StageContext } from "contexts/appStage";
import { useStatesContext } from "contexts/states";
import { Input } from "components/input";
import useMinMaxNumber from "hooks/useMinMaxNumber";
import { MeasurementsContext } from "contexts/measurements";
import DefaultButton from "components/Buttons/defaultButton";
import { theme } from "assets/styles/Theme";
import useNextScreen from "hooks/useNextScreen";
import maxDoorsWindows from "utils/maxDoorsWindows";

export default function Window() {
  const [, dispatch] = useContext(StageContext);
  const {
    window1,
    window2,
    window3,
    window4,
    setWindow1,
    setWindow2,
    setWindow3,
    setWindow4,
    doorTotal,
    setWindowTotal,
  } = useStatesContext();
  const formRef = useRef();
  const [measurements] = useContext(MeasurementsContext);

  function updateState(e, setWindow) {
    setWindow(Number(useMinMaxNumber(0, 100, e.target.value, setWindow)));
  }

  const total = window1 + window2 + window3 + window4;

  function nextScreen() {
    if (total * 3.2 * 100 + doorTotal > maxDoorsWindows(measurements)) {
      alert("O total de área das portas e janelas deve ser no máximo 50% da área de parede.");
    } else {
      setWindowTotal(total * 3.2 * 100);
      useNextScreen("", dispatch, "end");
    }
  }

  return (
    <>
      <Styled.Title>
        <h1>Tudo certo...</h1>
        <h2>E por último eu vou precisar saber quantas janelas existem na sua sala, tudo bem?</h2>
      </Styled.Title>
      <Styled.Form ref={formRef}>
        <Styled.Content>
          <Styled.InputContent>
            <h2>Janelas na 1º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={window1}
              placeholder="Digite aqui o número de janelas que existem na sua sala"
              onChange={(e) => updateState(e, setWindow1)}
            />
          </Styled.InputContent>
          <Styled.InputContent>
            <h2>Janelas na 2º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={window2}
              placeholder="Digite aqui o número de janelas que existem na sua sala"
              onChange={(e) => updateState(e, setWindow2)}
            />
          </Styled.InputContent>
        </Styled.Content>
        <Styled.Content>
          <Styled.InputContent>
            <h2>Janelas na 3º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={window3}
              placeholder="Digite aqui o número de janelas que existem na sua sala"
              onChange={(e) => updateState(e, setWindow3)}
            />
          </Styled.InputContent>
          <Styled.InputContent>
            <h2>Janelas na 4º parede:</h2>
            <Input
              type={"number"}
              min={0}
              max={100}
              value={window4}
              placeholder="Digite aqui o número de janelas que existem na sua sala"
              onChange={(e) => updateState(e, setWindow4)}
            />
          </Styled.InputContent>
        </Styled.Content>
      </Styled.Form>
      <Styled.ButtonDiv>
        <DefaultButton type={"button"} onClick={() => dispatch({ type: "door" })} background={theme.colors.warning}>
          Voltar
        </DefaultButton>
        <DefaultButton type={"button"} onClick={() => nextScreen()}>
          Avançar
        </DefaultButton>
      </Styled.ButtonDiv>
    </>
  );
}
