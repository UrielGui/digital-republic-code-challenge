import styled from "styled-components";
import { device } from "assets/styles/Breakpoints";

export const Content = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
  margin-bottom: 4rem;
  @media ${device.lg} {
    flex-direction: column;
    align-items: center;
  }
`;

export const ButtonDiv = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
  @media ${device.sm} {
    flex-direction: column;
    padding: 2rem;
  }
`;

export const Form = styled.form`
  width: 100%;
`;

export const Title = styled.div`
  line-height: 2;
  margin-bottom: 3rem;
  text-align: center;
  margin-left: 1rem;
  margin-right: 1rem;
`;

export const InputContent = styled.div`
  display: flex;
  flex-direction: column;
  background: #733fc5;
  padding: 30px 60px;
  line-height: 3;
  width: 30%;
  border-radius: 10px;
  opacity: ${(props) => (props.checked ? "50%" : "100%")};
  @media ${device.lg} {
    width: 80%;
    margin: 1rem;
  }
`;
