import styled from "styled-components";

export const Content = styled.div`
  padding-bottom: 1rem;
  text-align: center;

  h2 {
    padding-top: 2rem;
  }
`;
