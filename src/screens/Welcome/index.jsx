import React, { useContext } from "react";
import * as Styled from "./Styled";
import { StageContext } from "contexts/appStage";
import DefaultButton from "components/Buttons/defaultButton";
import useNextScreen from "hooks/useNextScreen";

export default function Welcome() {
  const [, dispatch] = useContext(StageContext);

  return (
    <>
      <Styled.Content>
        <h1>Calcule a quantidade de tinta necessária para pintar uma sala de 4 paredes</h1>
        <h2>Vamos começar?</h2>
      </Styled.Content>
      <DefaultButton type={"button"} onClick={() => useNextScreen("", dispatch, "wall")}>
        Começar
      </DefaultButton>
    </>
  );
}
