import styled from "styled-components";
import { device } from "assets/styles/Breakpoints";

export const Content = styled.div`
  padding-bottom: 4rem;
  text-align: center;

  h3 {
    margin-top: 1rem;

    @media ${device.lg} {
      margin-left: 1rem;
      margin-right: 1rem;
    }
  }
`;

export const ButtonDiv = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-around;
`;

export const CheckBox = styled.input`
  width: 30px;
  height: 30px;
  position: relative;
  top: 7px;
  right: 7px;
`;

export const Result = styled.div`
  display: flex;
  @media ${device.lg} {
    flex-direction: column;
    align-items: center;
  }
`;

export const Bucket = styled.div`
  display: block;
  padding: 3rem;
`;
