import React, { useContext, useState, useEffect } from "react";
import * as Styled from "./Styled";
import { MeasurementsContext } from "contexts/measurements";
import { useStatesContext } from "contexts/states";
import { GiEmptyMetalBucketHandle } from "react-icons/gi";

export default function End() {
  const [measurements] = useContext(MeasurementsContext);
  const { doorTotal, windowTotal } = useStatesContext();
  const [numberPaintCansNeededState1, setNumberPaintCansNeededState1] = useState(0);
  const [numberPaintCansNeededState2, setNumberPaintCansNeededState2] = useState(0);
  const [numberPaintCansNeededState3, setNumberPaintCansNeededState3] = useState(0);
  const [numberPaintCansNeededState4, setNumberPaintCansNeededState4] = useState(0);
  const [wallArea, setWallArea] = useState(0);
  // paint can size variations
  const canOfPaint1 = 0.5;
  const canOfPaint2 = 2.5;
  const canOfPaint3 = 3.6;
  const canOfPaint4 = 18;

  function totalMeasurementsforPaint() {
    const totalWallMeasurement =
      measurements.squareMeterWall1 +
      measurements.squareMeterWall2 +
      measurements.squareMeterWall3 +
      measurements.squareMeterWall4;
    const totalWallMeasurementAvailable = totalWallMeasurement * 100 - doorTotal - windowTotal;
    return totalWallMeasurementAvailable;
  }

  function paintCansNeeded() {
    const quantityLitersPaint = totalMeasurementsforPaint() / 100 / 5;
    setWallArea(totalMeasurementsforPaint() / 100);
    var restant2 = 0;
    var restant3 = 0;
    var restant4 = quantityLitersPaint;
    var numberPaintCansNeeded1 = 0;
    var numberPaintCansNeeded2 = 0;
    var numberPaintCansNeeded3 = 0;
    var numberPaintCansNeeded4 = 0;
    const numberValue = (val1, val2) => {
      const number = val1 - val2;
      return number.toFixed(1);
    };

    for (let i = canOfPaint4; i < quantityLitersPaint; i += canOfPaint4) {
      numberPaintCansNeeded4 = numberPaintCansNeeded4 + 1;
      restant4 = numberValue(quantityLitersPaint, i);
    }
    for (let i = canOfPaint3; i <= restant4; i += canOfPaint3) {
      numberPaintCansNeeded3 = numberPaintCansNeeded3 + 1;
      restant3 = numberValue(restant4, i);
    }
    if (restant3 === 0) {
      restant3 = restant4;
    }
    for (let i = canOfPaint2; i <= restant3; i += canOfPaint2) {
      numberPaintCansNeeded2 = numberPaintCansNeeded2 + 1;
      restant2 = numberValue(restant3, i);
    }
    if (restant2 === 0) {
      restant2 = restant3;
    }
    for (let i = canOfPaint1; i <= restant2; i += canOfPaint1) {
      numberPaintCansNeeded1 = numberPaintCansNeeded1 + 1;
    }
    setNumberPaintCansNeededState1(numberPaintCansNeeded1);
    setNumberPaintCansNeededState2(numberPaintCansNeeded2);
    setNumberPaintCansNeededState3(numberPaintCansNeeded3);
    setNumberPaintCansNeededState4(numberPaintCansNeeded4);

    return quantityLitersPaint.toFixed(1);
  }

  useEffect(() => {
    paintCansNeeded();
  });

  const Bucket = (qty, value) => {
    return (
      <Styled.Bucket>
        <h3>Balde de {qty} Litros</h3>
        <GiEmptyMetalBucketHandle size={70} />
        <p>Quantidade:</p>
        <h2>{value}</h2>
      </Styled.Bucket>
    );
  };

  return (
    <>
      <Styled.Content>
        <h1>Resultado</h1>
        <h3>Para pintar uma área de {wallArea}m, excluindo a área das portas e janelas, serão necessários:</h3>
        <Styled.Result>
          {Bucket(18, numberPaintCansNeededState4)}
          {Bucket("3,6", numberPaintCansNeededState3)}
          {Bucket("2,5", numberPaintCansNeededState2)}
          {Bucket("0,5", numberPaintCansNeededState1)}
        </Styled.Result>
        <small>*Cada litro de tinta é capaz de pintar uma área de 5 metros.</small>
      </Styled.Content>
    </>
  );
}
