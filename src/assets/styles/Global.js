import { createGlobalStyle } from "styled-components";
import { theme } from "assets/styles/Theme";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
  }
  body {
    background: ${theme.colors.background};
    color: ${theme.colors.white};
    font-family: ${theme.fonts.openSans};
  }
  button {
    border: none;
    cursor: pointer;
  }
  input {
    outline: none;
    border: 0;
  }
`;

export default GlobalStyle;
