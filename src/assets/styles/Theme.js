export const theme = {
  colors: {
    primary: "#472183",
    secondary: "#2C74B3",
    background: "#200e3c",
    text: "#fff",
    inputPlaceholder: "#0000005e",
    black: "#000000",
    white: "#ffffff",
    danger: "#ff7048",
    success: "#10A19D",
    warning: "#f4a358",
    warningLight: "#f4e158",
  },
  fonts: {
    openSans: "Open-Sans, Helvetica, Sans-Serif;",
  },
  paddings: {
    container: "15px",
    pageTop: "30px",
  },
  margins: {
    pageTop: "30px",
  },
};
