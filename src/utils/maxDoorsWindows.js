export default function maxDoorsWindows(measurements) {
  const wallMeasurement =
    measurements.squareMeterWall1 +
    measurements.squareMeterWall2 +
    measurements.squareMeterWall3 +
    measurements.squareMeterWall4;
  const fiftyPercent = wallMeasurement / 2;
  return fiftyPercent * 100;
}
