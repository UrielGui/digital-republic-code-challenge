import React, { createContext, useContext, useState } from "react";
import PropTypes from "prop-types";

export const StatesContext = createContext();

export const StatesProvider = (props) => {
  const [door1, setDoor1] = useState(0);
  const [door2, setDoor2] = useState(0);
  const [door3, setDoor3] = useState(0);
  const [door4, setDoor4] = useState(0);
  const [doorTotal, setDoorTotal] = useState(0);
  const [window1, setWindow1] = useState(0);
  const [window2, setWindow2] = useState(0);
  const [window3, setWindow3] = useState(0);
  const [window4, setWindow4] = useState(0);
  const [windowTotal, setWindowTotal] = useState(0);
  const values = {
    door1,
    door2,
    door3,
    door4,
    setDoor1,
    setDoor2,
    setDoor3,
    setDoor4,
    doorTotal,
    setDoorTotal,
    window1,
    window2,
    window3,
    window4,
    setWindow1,
    setWindow2,
    setWindow3,
    setWindow4,
    windowTotal,
    setWindowTotal,
  };
  return <StatesContext.Provider value={values}>{props.children}</StatesContext.Provider>;
};

export const useStatesContext = () => {
  const context = useContext(StatesContext);
  return context;
};

StatesProvider.propTypes = { children: PropTypes.object };
