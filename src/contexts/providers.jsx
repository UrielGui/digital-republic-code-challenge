import React from "react";
import PropTypes from "prop-types";
import { StageProvider } from "contexts/appStage";
import { MeasurementsProvider } from "contexts/measurements";
import { StatesProvider } from "contexts/states";

export default function Providers(props) {
  return (
    <>
      <StatesProvider>
        <StageProvider>
          <MeasurementsProvider>{props.children}</MeasurementsProvider>
        </StageProvider>
      </StatesProvider>
    </>
  );
}

Providers.propTypes = { children: PropTypes.array };
