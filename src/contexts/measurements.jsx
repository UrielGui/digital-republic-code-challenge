import React, { useReducer, createContext } from "react";
import PropTypes from "prop-types";

const initialFormState = {
  wallHeight1: 0,
  wallHeight2: 0,
  wallHeight3: 0,
  wallHeight4: 0,
  wallWidth1: 0,
  wallWidth2: 0,
  wallWidth3: 0,
  wallWidth4: 0,
  squareMeterWall1: 0,
  squareMeterWall2: 0,
  squareMeterWall3: 0,
  squareMeterWall4: 0,
};

function measurementsReducer(state, action) {
  const options = {
    ...state,
    [action.field]: Number(action.payload),
  };

  if (action.field) return options;
}

export const MeasurementsContext = createContext();

export const MeasurementsProvider = (props) => {
  const value = useReducer(measurementsReducer, initialFormState);

  return <MeasurementsContext.Provider value={value}>{props.children}</MeasurementsContext.Provider>;
};

MeasurementsProvider.propTypes = { children: PropTypes.array };
