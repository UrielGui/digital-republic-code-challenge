import React, { useReducer, createContext } from "react";
import PropTypes from "prop-types";

const STAGES = ["Start", "Wall", "Door", "Window", "End"];
const initialState = { appStage: STAGES[0] };

function stageReducer(state, action) {
  const options = {
    start: { ...state, appStage: STAGES[0] },
    wall: { ...state, appStage: STAGES[1] },
    door: { ...state, appStage: STAGES[2] },
    window: { ...state, appStage: STAGES[3] },
    end: { ...state, appStage: STAGES[4] },
  };

  if (action.type) return options[action.type];
}

export const StageContext = createContext();

export const StageProvider = (props) => {
  const value = useReducer(stageReducer, initialState);

  return <StageContext.Provider value={value}>{props.children}</StageContext.Provider>;
};

StageProvider.propTypes = { children: PropTypes.object };
