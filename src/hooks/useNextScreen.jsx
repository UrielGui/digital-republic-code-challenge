export const useNextScreen = (checkObj, dispatch, nextPage) => {
  if (!Object.values(checkObj).includes(0)) {
    dispatch({ type: nextPage });
  } else {
    alert("É necessário preencher os campos corretamente.");
  }
};

export default useNextScreen;
