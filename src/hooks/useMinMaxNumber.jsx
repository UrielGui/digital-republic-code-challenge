export const useMinMaxNumber = (min, max, value, setState) => {
  if (value > max) {
    setState ? setState(max) : null;
    return max;
  }
  if (value < min) {
    setState ? setState(max) : null;
    return min;
  }
  setState ? setState(value) : null;
  return value;
};

export default useMinMaxNumber;
